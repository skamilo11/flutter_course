import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import 'models/image_model.dart';
import 'dart:convert';
import 'widgets/image_list.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> {
  int counter = 0;
  String url = "http://jsonplaceholder.typicode.com/photos/";
  List<ImageModel> images = [];

  fetchImage() async {
    setState(() => counter++);
    var response = await get("$url$counter");
    var image = ImageModel.fromJson(json.decode(response.body));
    setState(() => images.add(image));
  }

  Widget build(context) {
    return MaterialApp(
      home: Scaffold(
        body: ImagesList(images),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: fetchImage,
        ),
        appBar: AppBar(
          title: Text('Lets see some images!'),
        ),
      ),
    );
  }
}
