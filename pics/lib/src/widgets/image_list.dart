import 'package:flutter/material.dart';
import '../models/image_model.dart';

class ImagesList extends StatelessWidget {
  final List<ImageModel> images;

  ImagesList(this.images);

  Widget build(context) {
    return ListView.builder(
      itemCount: images.length,
      itemBuilder: (context, int index) {
        return buildImage(images[index]);
      },
    );
  }

  Widget buildImage(ImageModel image) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
      margin: EdgeInsets.all(20.0),
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          Padding(
            child: Image.network(image.url),
            padding: EdgeInsets.only(
              bottom: 10,
            ),
          ),
          Text(image.title),
        ],
      ),
    );
  }
}
